





var path = require('path');
var Express = require('express');
var fs = require('fs');
var bodyParser = require('body-parser');
var shortid = require('shortid');
var TTS = require('./lib/tts');
var monitorAndClean = require('./lib/monitor-and-clean');
var cors = require('cors');

var filesDir = path.resolve('files');

monitorAndClean(filesDir, {
    expirationTime: 30000
});


var app = Express();
app.use(cors());
app.use( Express.static( filesDir ));
app.use(bodyParser.json());



app.post('/tts', function(req, res){
    res.header('Access-Control-Allow-Origin', '*');
    var text = req.body.text;
    var voice = req.body.voice;
    var fname = shortid.generate();

    TTS.say({

        text: text,
        voice: voice,
        targetFileName: fname,
        targetDirPath: filesDir,
        loud: false,

    }, function(fname){

        res.json(fname);

    });

});

app.listen(3003, function(){
    console.log("tts is up");
});

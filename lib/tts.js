

var childProcess = require('child_process');
var path = require('path');

const spawn = childProcess.spawn;

module.exports =   {
    say: function(spec, callback){

        var text = spec.text;
        var voice = spec.voice;
        var targetDirPath = spec.targetDirPath;
        var targetFileName = spec.targetFileName;
        var loud = spec.loud;



        targetFileName = targetFileName  + ".wav";
        var outFilePath = path.join(targetDirPath, targetFileName);

        var commands = [    '-v', voice,
                            text
        ];
        
        if(!loud){
            commands = commands.concat( [    '--file-format=WAVE','',
                                             '--data-format=LEF32@32000',
                                             '-o', outFilePath
                                        ] )
        }

        var say = spawn('say', commands);

        say.stderr.once('data', function(data)  { throw new Error(data) });

        say.stdout.on('data', function(data)  {console.log('out', data)});

        say.on('exit', function(code) {
            if(callback){
                if(!loud){
                    callback(targetFileName);
                } else {
                    callback();
                }
            }
        });
    }
};
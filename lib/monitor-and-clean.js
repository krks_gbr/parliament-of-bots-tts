


var fs = require('fs');
var path = require('path');




module.exports = function(dirPath, config){

    var expirationTime = config.expirationTime;


    //run cleaning procedure every 10 seconds
    setInterval(function(){

        // list files in directory
        fs.readdir(dirPath, function(err, files){

            if(err){
                return console.log('cannot read dir', dirPath);
            }



            // fs.stat and unlink might throw errors
            try {

                files
                //get rid of .DS_Store
                    .filter(function (fname) {
                        return fname !== '.DS_Store'
                    })

                    // map fnames to full paths
                    .map(function (fname) {
                        return path.join(dirPath, fname)
                    })

                    //filter out expired files
                    .filter(function (fpath) {

                        var stats = fs.statSync(fpath);

                        var aTime = new Date(stats.atime);
                        var currentTime = Date.now();

                        return currentTime - aTime > expirationTime;

                    })

                    //delete each expired file
                    .forEach(function (fpath) {
                        fs.unlinkSync(fpath);
                        console.log('removed', fpath, 'because it expired');
                    });

            } catch(e) {

                console.log(e);

            }



        }, 1000 );


    })

;


};